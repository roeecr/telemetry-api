﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TelemetryApi.Models;
using TelemetryApi.InfluxDBManager;

namespace TelemetryApi.Controllers
{
    [EnableCors("_myAllowSpecificOrigins")]
    [Route("TelemetryApi")]
    [ApiController]
    public class TelemetryController : ControllerBase
    {
        private readonly InfluxDBExecute executer;

        public TelemetryController()
        {
            executer = new InfluxDBExecute();
        }


        [HttpGet("{device}")]
        public async Task<List<Sensor>> GetAllSensors(string device)
        {
            Console.WriteLine("GetAllSensors");

            List<Sensor> items = await executer.GetSensors(device);

            Console.WriteLine("Sending obj\n\n");
            return items;
        }

        [HttpGet]
        [HttpGet("{device}/{key}")]
        public async Task<TelemetryItem[]> GetTelemetryByKeyAndTime(string device, string key, string name, string time, string stop)
        {
            Console.WriteLine("GetTelemetryByKeyAndTime");
            Console.WriteLine($"device={device} key={key} name={name} time={time}, stop={stop}");

            //TelemetryItem[] items = await executer.GetTelemetryItems(device, key, time, stop);
            TelemetryItem[] items = await executer.GetTelemetryItemsWindow(device, key, name, time, stop);

            Console.WriteLine("Sending obj\n\n");
            return items;
        }




        //[HttpGet]
        //public async Task<TelemetryItem[]> GetTelemetryByKeyAndTime(string device, string key, string time, string stop)
        //{
        //    Console.WriteLine("GetTelemetryByKeyAndTime");
        //    Console.WriteLine($"device={device} key={key} time={time}, stop={stop}");

        //    TelemetryItem[] items = await executer.GetTelemetryItems(device, key, time, stop);

        //    Console.WriteLine("Sending obj\n\n");
        //    return items;
        //}


        //[HttpGet]
        //public async Task<Sensor[]> GetAllSensors(string device)
        //{
        //    Console.WriteLine("GetTodoItems");

        //    Sensor[] items = await executer.GetSensors(device);

        //    Console.WriteLine("Sending obj\n\n");
        //    return items;
        //}
    }
}
