﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TelemetryApi.Models;

namespace TelemetryApi.InfluxDBManager
{
    public class InfluxDBExecute
    {
        private InfluxDBConnection connection;

        public InfluxDBExecute()
        {
            this.connection = InfluxDBConnection.getInstance();
        }

        #region get telemetry by key and time
        public async Task<TelemetryItem[]> GetTelemetryItems(string device, string key, string time, string stop)
        {
            if (stop != "now()")
                stop = $"-{stop}";

            Console.WriteLine("*** Query Points ***");

            Console.WriteLine($"device={device}, key={key}, time={time}, stop={stop}");

            var query = $"from(bucket: \"{connection.getDatabaseName()}/{connection.getRetentionPolicy()}\")\n"
                + $" |> range(start: -{time}, stop: {stop})"
                + $" |> filter(fn: (r) => (r[\"_measurement\"] == \"{device}\" and r[\"sensorKey\"] == \"{key}\"))";

            Console.WriteLine(query);

            var fluxTables = await connection.queryApi.QueryAsync(query);

            return TableToTelemetryItems(fluxTables);
        }

        private TelemetryItem[] TableToTelemetryItems(List<InfluxDB.Client.Core.Flux.Domain.FluxTable> fluxTables)
        {
            var nameRecords = fluxTables[0].Records;
            var valueRecords = fluxTables[1].Records;
            //var records = fluxTables[0].Records;

            TelemetryItem[] items = new TelemetryItem[valueRecords.Count];
            for (int i = 0; i < valueRecords.Count; i++)
            {
                try
                {
                    var nameRec = nameRecords[i];
                    var valRec = valueRecords[i];
                    string measurementRec = valRec.GetMeasurement();
                    string sensorKey = valRec.GetValueByKey("sensorKey").ToString();
                    string timeRec = ((DateTime)valRec.GetTimeInDateTime()).ToString("yyyy-MM-dd HH:mm:ss.FFF");
                    string sensorValue = valRec.GetValue().ToString();
                    string sensorName = nameRec.GetValue().ToString();

                    TelemetryItem item = new TelemetryItem();
                    item.Device = measurementRec; item.Key = sensorKey; item.Value = sensorValue; item.Name = sensorName; item.Time = timeRec;
                    //Console.WriteLine($"Time:{item.Time}, Measurement: {item.Device}. Sensor {item.Name} -> {item.Value}");
                    //Console.ReadLine();
                
                    items[i] = item;
                }
                catch
                {
                    Console.WriteLine($"Error in record {i}");
                    throw new Exception($"Error in record {i}");
                }
            }

            return items;
        }

        #endregion

        #region get telemetry using window

        public async Task<TelemetryItem[]> GetTelemetryItemsWindow(string device, string key, string name, string time, string stop)
        {
            string startRange = this.GetStartRange(time);
            string stopRange = this.GetStopRange(stop);

            Console.WriteLine("*** Query Points ***");

            string sensor = await this.GetSensorName(key, device);

            Console.WriteLine($"device={device}, key={key}, name={name}, time={time}, stop={stop}");

            var query = $"from(bucket: \"{connection.getDatabaseName()}/{connection.getRetentionPolicy()}\")\n"
                + $" |> range(start: {startRange}, stop: {stopRange})"
                + $" |> filter(fn: (r) => (r[\"_measurement\"] == \"{device}\" and r[\"sensorKey\"] == \"{key}\" and r[\"_field\"] == \"value\"))"
                + $" |> window(every: 1m)";

            Console.WriteLine(query);

            var fluxTables = await connection.queryApi.QueryAsync(query);

            return TableToTelemetryItemsWindow(fluxTables, sensor, name);
        }

        private TelemetryItem[] TableToTelemetryItemsWindow(List<InfluxDB.Client.Core.Flux.Domain.FluxTable> fluxTables, string sensorName, string comp)
        {
            int samples = 1;
            Random random = new Random();
            TelemetryItem[] items = new TelemetryItem[fluxTables.Count * samples];
            int randomNumber;

            for (int i = 0; i < fluxTables.Count; i++)
            {
                try
                {
                    var records = fluxTables[i].Records;
                    for (int j = 0; j < samples; j++)
                    {
                        randomNumber = random.Next(0, records.Count);

                        var record = records[randomNumber];
                        string measurementRec = record.GetMeasurement();
                        string sensorKey = record.GetValueByKey("sensorKey").ToString();
                        string sensorValue = record.GetValue().ToString();
                        string timeRec = ((DateTime)record.GetTimeInDateTime()).ToString("yyyy-MM-dd HH:mm");
                        //string timeRec = ((DateTime)record.GetTimeInDateTime()).ToString("yyyy-MM-dd HH:mm:ss.FFF");

                        TelemetryItem item = new TelemetryItem();
                        item.Device = measurementRec; item.Key = sensorKey; item.Time = timeRec;
                        //Console.WriteLine($"Time:{item.Time}, Measurement: {item.Device}. Sensor {item.Name} -> {item.Value}");
                        //Console.ReadLine();

                        if (sensorKey == "GPS5")
                        {
                            string compString = sensorName.Split(new string[] { "(", ")" }, 3, StringSplitOptions.None)[1];
                            string[] separatingStrings = { ", "};
                            string[] comps = compString.Split(separatingStrings, StringSplitOptions.RemoveEmptyEntries);
                            string[] values = sensorValue.Split(separatingStrings, StringSplitOptions.RemoveEmptyEntries); ;
                            int index = Array.IndexOf(comps, comp);
                            item.Value = values[index]; item.Name = comp;
                        }
                        else
                        {
                            item.Value = sensorValue; item.Name = sensorName;
                        }

                        items[i * samples + j] = item;
                    }
                }
                catch
                {
                    Console.WriteLine($"Error in record {i}");
                    throw new Exception($"Error in record {i}");
                }
            }

            return items;
        }

        public async Task<string> GetSensorName(string name, string measurement)
        {
            Console.WriteLine("*** Query Points ***");

            Console.WriteLine($"device={measurement}, name={name}");

            var query = $"from(bucket: \"{connection.getDatabaseName()}/{connection.getRetentionPolicy()}\")\n"
                + $" |> range(start: -0)"
                //+ $" |> filter(fn: (r) => (r[\"_measurement\"] == \"{this.measurement}\" and r[\"sensorKey\"] == \"{name}\" and r[\"_field\"] == \"value\"))"
                + $" |> filter(fn: (r) => (r[\"_measurement\"] == \"{measurement}\" and r[\"sensorKey\"] == \"{name}\" and r[\"_field\"] == \"sensorName\"))"
                + $" |> limit(n: 1)";



            //+ $" |> group(columns: [\"_time\"], mode: \"by\")";
            //+ $" |> group(columns: [\"sensorKey\"], mode: \"by\")";


            Console.WriteLine(query);

            var fluxTables = await connection.queryApi.QueryAsync(query);
            var record = fluxTables[0].Records[0];
            return record.GetValue().ToString();
        }

        public string GetStartRange(string time)
        {
            string range;
            if (time == "1m")
            {
                DateTime ago = DateTime.UtcNow.AddMonths(-1);
                var datetimeoffset = new DateTimeOffset(ago);
                var unixdatetime = datetimeoffset.ToUnixTimeSeconds();
                range = unixdatetime.ToString();
            }
            else
            {
                range = $"-{time}";
            }

            return range;
        }

        public string GetStopRange(string time)
        {
            string range;
            if (time == "now")
            {
                range = $"{time}()";
            }
            else
            {
                range = $"-{time}";
            }

            return range;
        }
        #endregion

        #region get sensor by device

        public async Task<List<Sensor>> GetSensors(string device)
        {
             Console.WriteLine("*** Query Points ***");

            Console.WriteLine($"device={device}");

            var query = $"from(bucket: \"{connection.getDatabaseName()}/{connection.getRetentionPolicy()}\")\n"
                + $" |> range(start: -0)"
                + $" |> filter(fn: (r) => (r[\"_measurement\"] == \"{device}\"))"
                + $" |> group(columns: [\"sensorKey\"], mode: \"by\")";

            Console.WriteLine(query);

            var fluxTables = await connection.queryApi.QueryAsync(query);

            return TableToSensors(fluxTables);
        }

        private List<Sensor> TableToSensors(List<InfluxDB.Client.Core.Flux.Domain.FluxTable> fluxTables)
        {
            List<Sensor> items = new List<Sensor>();
            //Sensor[] items = new Sensor[fluxTables.Count];
            for (int i = 0; i < fluxTables.Count; i++)
            {
                try
                {
                    var records = fluxTables[i].Records;
                    var record = records[0];
                    string sensorKey = record.GetValueByKey("sensorKey").ToString();
                    string sensorName;

                    if (record.GetField().ToString() != "sensorName")
                        record = records[records.Count - 1];

                    sensorName = record.GetValue().ToString();


                    Sensor item = new Sensor();
                    item.Key = sensorKey; item.Name = sensorName;
                    
                    if (!items.Contains(item))
                    {
                        items.Add(item);
                    }
                }
                catch
                {
                    Console.WriteLine($"Error in record {i}");
                    throw new Exception($"Error in record {i}");
                }
            }

            return items;
        }

        #endregion
    }
}
